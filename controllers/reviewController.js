const mongoose = require('mongoose');
const Review = mongoose.model('Review');

exports.addReview = async (req, res) => {
  req.body.author = req.user._id;
  req.body.image = req.params.id;
  // res.json(req.body);
  const newReview = new Review(req.body);
  await newReview.save();
  req.flash('success', 'Вашето ревю бе записано успешно!');
  res.redirect('back');
};
