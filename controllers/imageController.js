const mongoose = require('mongoose');
const Image = mongoose.model('Image');
const User = mongoose.model('User');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

// options for multer (package that handles photo upload functionality)
const multerOptions = {
  storage: multer.memoryStorage(),
  fileFilter(req, file, next) {
    const isPhoto = file.mimetype.startsWith('image/');

    if (isPhoto) {
      next(null, true);
    } else {
      next({ message: 'That filetype isn\'t allowed!' }, false);
    }
  }
};

// renders home page
exports.homePage = (req, res) => {
  res.render('index');
};

// renders add/edit image page
exports.addImage = (req, res) => {
  res.render('editImage', { title: 'Нова снимка' });
};

// reads image into memory
exports.upload = multer(multerOptions).single('photo');

// middleware that resizes image in case it is too big (prevents user from uploading 5MBs+ files)
exports.resize = async (req, res, next) => {
  // check if there is no new file to resize
  if (!req.file) {
    next();
    return;
  }

  const extension = req.file.mimetype.split('/')[1];

  // putting a photo on the body will save it to DB, because we save req.body
  req.body.photo = `${uuid.v4()}.${extension}`;

  // resize
  const photo = await jimp.read(req.file.buffer);
  await photo.resize(1200, jimp.AUTO);
  await photo.write(`./public/uploads/${req.body.photo}`);

  // once we have written the photo to our filesystem, keep going!
  next();
};

// create an image card and save it to DB
exports.createImage = async (req, res) => {
  req.body.author = req.user._id;

  const image = await (new Image(req.body)).save();

  req.flash('success', `Успешно създаване на снимка със заглавие ${image.image}.`);
  res.redirect(`/image/${image.slug}`);
};

// get images' cards to display on a single page (index, images pages)
exports.getImages = async (req, res) => {
  const page = req.params.page || 1;
  const limit = 6;
  const skip = (page * limit) - limit;

  // 1. Query the database for a list of all images
  const imagesPromise = Image
    .find()
    .skip(skip)
    .limit(limit)
    .sort({ created: 'desc' });

  const countPromise = Image.count();

  const [images, count] = await Promise.all([imagesPromise, countPromise]);
  const pages = Math.ceil(count / limit);

  if (!images.length && skip) {
    req.flash('info', `Хей! Вие потърсихте страница ${page}. Тъй като такава страница не същестува ви изпратихме на страница ${pages}`);
    res.redirect(`/images/page/${pages}`);
    return;
  }

  res.render('images', {
    title: 'Снимки',
    images: images,
    page,
    pages,
    count
  });
};

// check if current user is owner of image card (privileges to edit it?)
const confirmOwner = (image, user) => {
  if (!image.author.equals(user._id)) {
    throw Error('You must own a image in order to edit it!');
  }
};

// edit existing image card
exports.editImage = async (req, res) => {
  // 1. Find the image given the ID
  const image = await Image.findOne({ _id: req.params.id });

  // 2. confirm they are the owner of the image (admin?)
  confirmOwner(image, req.user);

  // 3. Render out the edit form so the user can update their image
  res.render('editImage', { title: `Редактирай ${image.image}`, image: image });
};

// update existing image card
exports.updateImage = async (req, res) => {
  // set the location data to be a point
  req.body.location.type = 'Point';

  // find and update the image card
  const image = await Image.findOneAndUpdate(
    { _id: req.params.id }, 
    req.body, 
    {
      new: true, // return the new image instead of the old one
      runValidators: true
    }
  ).exec();

  // Redriect creator to the image and tell them it worked
  req.flash('success', `Успешно актуализирана снимка <strong>${image.image}</strong>. <a href="/image/${image.slug}">Виж снимка →</a>`);
  res.redirect(`/images/${image._id}/edit`);
};

// delete image from DB
exports.deleteImage = async (req, res) => {
  const image = await Image.remove(
    { _id: req.params.id }
  ).exec();

  req.flash('success', `Успешно изтриване на снимка.`);
  res.json(image);
};

// gets image card by slug (url friendly name) - used when viewing an image card
exports.getImageBySlug = async (req, res, next) => {
  const image = await Image
    .findOne({
      slug: req.params.slug
    })
    .populate('author reviews');

  if (!image) {
    next();
    return;
  }

  res.render('image', { image: image, title: image.image });
};

// filter by category/category
exports.getImagesByCategory = async (req, res) => {
  const category = req.params.category;
  const categoryQuery = category || { $exists: true, $ne: [] };

  const categoriesPromise = Image.getCategoriesList();
  const imagesPromise = Image.find({ categories: categoryQuery });
  const [categories, images] = await Promise.all([categoriesPromise, imagesPromise]);


  res.render('category', { categories, title: 'Категории', category, images });
};


exports.searchImages = async (req, res) => {
  const images = await Image
  // first find images that match
  .find({
    $text: {
      $search: req.query.q
    }
  }, {
    score: { $meta: 'textScore' }
  })
  // the sort them
  .sort({
    score: { $meta: 'textScore' }
  })
  // limit to only 5 results
  .limit(5);
  res.json(images);
};

exports.mapImages = async (req, res) => {
  const coordinates = [req.query.lng, req.query.lat].map(parseFloat);
  const q = {
    location: {
      $near: {
        $geometry: {
          type: 'Point',
          coordinates
        },
        $maxDistance: 100000000 // 100000km (earth is 12742 km wide overall so it should fit all earth)
      }
    }
  };

  const images = await Image
    .find(q)
    .select('slug image description location photo') // whitelist of fields to return from query
    .limit(100);
  res.json(images);
};

exports.mapPage = (req, res) => {
  res.render('map', { title: 'Карта' });
};

// 'hearts' image to save it for later
exports.heartImage = async (req, res) => {
  const hearts = req.user.hearts.map(obj => obj.toString());

  const operator = hearts.includes(req.params.id) ? '$pull' : '$addToSet';
  const user = await User
    .findByIdAndUpdate(req.user._id,
      { [operator]: { hearts: req.params.id } },
      { new: true }
    );
  res.json(user);
};

// fetches all images that user has 'hearted'
exports.getHearts = async (req, res) => {
  const images = await Image.find({
    _id: { $in: req.user.hearts }
  });

  res.render('images', { title: 'Харесани снимки', images: images });
};

exports.getTopImages = async (req, res) => {
  const images = await Image.getTopImages();
  // res.json(images);
  res.render('topImages', {
    images: images,
    title: 'Топ снимки',
  });
};