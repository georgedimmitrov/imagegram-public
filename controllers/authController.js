const passport = require('passport');
const crypto = require('crypto');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');
const mail = require('../handlers/mail');

// log user in
exports.login = passport.authenticate('local', {
  failureRedirect: '/login',
  failureFlash: 'Неуспешно вписване в системата!',
  successRedirect: '/',
  successFlash: 'Успешно вписване в системата!'
});

// log user out
exports.logout = (req, res) => {
  req.logout();
  req.flash('success', 'Успешно излязохте!');
  res.redirect('/');
};

// check if user is logged in
exports.isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    next();
    return;
  }

  req.flash('error', 'Упс, трябва да се впишете с вашия потребителски профил, за да извършите това действие!');
  res.redirect('/login');
};

// user forgot password
exports.forgot = async (req, res) => {
  // 1. See if a user with that email exists
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    req.flash('error', 'Не успяхме да намерим акаунт с този email адрес.');
    return res.redirect('/login');
  }

  // 2. Set reset tokens and expiry on their account
  user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
  user.resetPasswordExpires = Date.now() + 3600000; // 1 hour from now
  await user.save();

  // 3. Send them an email with the token
  const resetURL = `http://${req.headers.host}/account/reset/${user.resetPasswordToken}`;

  await mail.send({
    user,
    filename: 'password-reset',
    subject: 'Password Reset',
    resetURL
  });

  req.flash('success', `Беше ви изпратен мейл с линк за възстановяване на паролата.`);

  // 4. redirect to login page
  res.redirect('/login');
};

// used  to reset user's password
exports.reset = async (req, res) => {
  // find user that has the password reset token and the expires is greater than now
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });

  if (!user) {
    req.flash('error', 'Възстановяването на паролата е невалидно или е изтекло!');
    return res.redirect('/login');
  }

  // if there is a user, show the reset password form
  res.render('reset', { title: 'Променете паролата си' });
};

// middleware that checks if passwords match exactly
exports.confirmedPasswords = (req, res, next) => {
  if (req.body.password === req.body['password-confirm']) {
    next();
    return;
  }
  req.flash('error', 'Паролите не съвпадат!');
  res.redirect('back');
};

// update user's password in DB
exports.update = async (req, res) => {
  // find user that has the password reset token and the expires is greater than now
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });

  if (!user) {
    req.flash('error', 'Възстановяването на паролата е невалидно или е изтекло.');
    return res.redirect('/login');
  }

  const setPassword = promisify(user.setPassword, user);

  await setPassword(req.body.password);

  // get rid of unneeded fields in DB
  user.resetPasswordToken = undefined;
  user.resetPasswordExpires = undefined;

  // save updated user to DB
  const updatedUser = await user.save();

  // login user automatically using passport's login method
  await req.login(updatedUser);

  req.flash('success', 'Вашата парола бе променена! Вписахме ви автоматично!');
  res.redirect('/');
};
