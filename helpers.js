const fs = require('fs');

// moment.js is a handy library for displaying dates. We need this in our templates to display things like "Posted 5 minutes ago"
exports.moment = require('moment');

// "console.log" data in a template
exports.dump = (obj) => JSON.stringify(obj, null, 2);

exports.staticMap = ([lng, lat]) => `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=6&size=800x250&key=${process.env.MAP_KEY}&markers=${lat},${lng}&scale=2`;

// inserting an SVG
exports.icon = (name) => fs.readFileSync(`./public/images/icons/${name}.svg`);

// Some details about the site
exports.siteName = `Imagegram`;

// nav
exports.menu = [
  { slug: '/images', title: 'Снимки' },
  { slug: '/categories', title: 'Категории' },
  { slug: '/top', title: 'Класация' },
  { slug: '/map', title: 'Карта' },
];

// admin nav
exports.adminMenu = [
  { slug: '/add', title: 'Качи снимка' }
];
