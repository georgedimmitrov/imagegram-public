### Imagegram - платформа за снимки

Приложение написано на Node.js за back-end и JavaScript за front-end, което позволява на потребителите да качват снимки като отличителните му черти са, че има интеграция с google maps и показва къде е заснета снимката и има интерактивна карта на света с всички снимки и тяхното местонахождение. Всеки регистриран потребител може да качва снимки и съответно да изтрива своите качени такива.

Проектът е разработен като дипломна работа за ТУ - София.

### Първа инсталация

```
npm install
```

### Полезни команди

При разработка

```
npm run dev
```

За добавяне на тестови данни:

```bash
npm run sample
```

За изтриване на тестови данни:

```bash
npm run deletesample
```

Горните команди ще добавят снимки, ревюта, както и няколко акаунта като отдолу може да видите техните потребителски имена и пароли:

| Name               | Email (login)  | Password |
| ------------------ | -------------- | -------- |
| Администратор Едно | admin@abv.bg   | admin    |
| Потребител Първи   | userone@abv.bg | userone  |
| Потребител Втори   | usertwo@abv.bg | usertwo  |
| Георги Димитров    | georgi@abv.bg   | georgi    |

Изглед на приложението:
![Alt text](/screenshots/0_nachalna_stranica.jpg?raw=true 'Начален екран на приложението')
