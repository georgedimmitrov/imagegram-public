require('dotenv').config({ path: __dirname + '/../variables.env' });
const fs = require('fs');

const mongoose = require('mongoose');
mongoose.connect(process.env.DATABASE);
mongoose.Promise = global.Promise;

// models need to be imported once
const Image = require('../models/Image');
const User = require('../models/User');
const Review = require('../models/Review');

const images = JSON.parse(fs.readFileSync(__dirname + '/images2.json', 'utf-8'));
const users = JSON.parse(fs.readFileSync(__dirname + '/users2.json', 'utf-8'));
const reviews = JSON.parse(fs.readFileSync(__dirname + '/reviews2.json', 'utf-8'));

async function deleteData() {
  await Image.remove();
  await User.remove();
  await Review.remove();
  console.log('Data Deleted.');
  process.exit();
}

async function loadData() {
  try {
    await Image.insertMany(images);
    await User.insertMany(users);
    await Review.insertMany(reviews);
    console.log('Data Inserted.');
    process.exit();
  } catch(e) {
    console.log('\n Error! Make sure to drop the existing database first with.\n\n\t npm run deletesample\n\n\n');
    console.log(e);
    process.exit();
  }
}
if (process.argv.includes('--delete')) {
  deleteData();
} else {
  loadData();
}
