const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const reviewSchema = new mongoose.Schema({
  created: {
    type: Date,
    default: Date.now()
  },
  author: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: 'Трябва да въведете автор!'
  },
  image: {
    type: mongoose.Schema.ObjectId,
    ref: 'Image',
    required: 'Трябва да въведете снимка!'
  },
  text: {
    type: String,
    required: 'Вашето ревю трябва да съдържа текст!'
  },
  rating: {
    type: Number,
    min: 1,
    max: 5
  }
});

function autopopulate(next) {
  this.populate('author');
  next();
}

// anytime somebody finds or findsOne author field is populated
reviewSchema.pre('find', autopopulate);
reviewSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Review', reviewSchema);
