const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

function transliterate(word){
  var answer = ""
    , a = {};

  a["Ё"]="YO";a["Й"]="I";a["Ц"]="TS";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="SH";a["Щ"]="SCH";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
  a["ё"]="yo";a["й"]="i";a["ц"]="ts";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="sh";a["щ"]="sch";a["з"]="z";a["х"]="h";a["ъ"]="'";
  a["Ф"]="F";a["Ы"]="I";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="ZH";a["Э"]="E";
  a["ф"]="f";a["ы"]="i";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="zh";a["э"]="e";
  a["Я"]="Ya";a["Ч"]="CH";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a["Ь"]="'";a["Б"]="B";a["Ю"]="YU";
  a["я"]="ya";a["ч"]="ch";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["ю"]="yu";

  for (i in word){
    if (word.hasOwnProperty(i)) {
      if (a[word[i]] === undefined){
        answer += word[i];
      } else {
        answer += a[word[i]];
      }
    }
  }
  return answer;
}

const imageSchema = new mongoose.Schema({
  image: {
    type: String,
    trim: true,
    required: 'Моля въведете заглавие!'
  },
  slug: String,
  description: {
    type: String,
    trim: true
  },
  categories: [String],
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: 'Трябва да въведете координати!'
    }],
    habitat: {
      type: String,
      required: 'Трябва да въведете местоположение!'
    }
  },
  photo: String,
  author: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: 'Трябва да въведете автор!'
  }
}, {
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

// Define our indexes
imageSchema.index({
  image: 'text',
  description: 'text'
});

imageSchema.index({ location: '2dsphere' });

imageSchema.pre('save', async function(next) {
  if (!this.isModified('image')) {
    next(); // skip it
    return;
  }
  this.slug = slug(transliterate(this.image));
  // find other images that have a slug of tiger, tiger-1, tiger-2
  const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
  const imagesWithSlug = await this.constructor.find({ slug: slugRegEx });
  if (imagesWithSlug.length) {
    this.slug = `${this.slug}-${imagesWithSlug.length + 1}`;
  }

  if (this.description && this.description.length) {
    this.description = this.description.replace(/\n/g, '<br />'); 
    // console.log(this.description);
  }

  next();
});

// get available categories for image
imageSchema.statics.getCategoriesList = function() {
  return this.aggregate([
    // returns instance of Image for each existing category (if 2 exist - 2 instances of Image obj {}, {}) - in result each image has one category
    { $unwind: '$categories' },
    // group everything based on category field and create a new field in each of those groups called count that 'sums' (increments) itself by 1 for each match
    { $group: { _id: '$categories', count: { $sum: 1 } } },
    // sort by count descending
    { $sort: { count: -1 } }
  ]);
};

imageSchema.statics.getTopImages = function() {
  return this.aggregate([
    // Lookup Images and populate their reviews
    {
      $lookup: {
        from: 'reviews', // model name is `Review` -> mongodb automatically lowercases and adds 's' at the end -> makes it `reviews`
        localField: '_id', // which field in Image
        foreignField: 'image', // which field in Review
        as: 'reviews' // what to name it
      }
    },
    // filter for only items that have 2 or more reviews
    // NOOB INTERPRETATION: where the second item index[1] in reviews exists
    {
      $match: {
        'reviews.1': { $exists: true }
      }
    },
    // Add the average reviews field
    {
      $project: {
        photo: '$$ROOT.photo',
        image: '$$ROOT.image',
        reviews: '$$ROOT.reviews',
        slug: '$$ROOT.slug',
        averageRating: { $avg: '$reviews.rating' }
      }
    },
    // sort it by our new field, highest reviews first
    { $sort: { averageRating: -1 } },
    // limit to at most 10
    { $limit: 10 },
  ]);
};

// find reviews where the image's _id property === reviews image property
imageSchema.virtual('reviews', {
  ref: 'Review', // what model to link?
  localField: '_id', // which field on the image?
  foreignField: 'image', // which field on the review?
});

function autopopulate(next) {
  this.populate('reviews');
  next();
}

imageSchema.pre('find', autopopulate);
imageSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Image', imageSchema);
