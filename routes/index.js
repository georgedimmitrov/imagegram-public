const express = require('express');
const router = express.Router();
const imageController = require('../controllers/imageController');
const userController = require('../controllers/userController');
const authController = require('../controllers/authController');
const reviewController = require('../controllers/reviewController');
const { catchErrors } = require('../handlers/errorHandlers');

router.get('/', catchErrors(imageController.getImages));
router.get('/images', catchErrors(imageController.getImages));
router.get('/images/page/:page', catchErrors(imageController.getImages));

// show /add page if user is logged in and admin
router.get('/add', 
  authController.isLoggedIn,
  imageController.addImage
);

router.post('/add',
  imageController.upload,
  catchErrors(imageController.resize),
  catchErrors(imageController.createImage)
);

router.post('/add/:id',
  imageController.upload,
  catchErrors(imageController.resize),
  catchErrors(imageController.updateImage)
);


router.get('/images/:id/edit', catchErrors(imageController.editImage));
router.get('/image/:slug', catchErrors(imageController.getImageBySlug));

router.get('/categories', catchErrors(imageController.getImagesByCategory));
router.get('/categories/:category', catchErrors(imageController.getImagesByCategory));

router.get('/login', userController.loginForm);
router.post('/login', authController.login);
router.get('/register', userController.registerForm);

// 1. Validate the registration data
// 2. register the user
// 3. we need to log them in
router.post('/register',
  userController.validateRegister,
  userController.register,
  authController.login
);

router.get('/logout', authController.logout);

router.get('/account', authController.isLoggedIn, userController.account);
router.post('/account', catchErrors(userController.updateAccount));
router.post('/account/forgot', catchErrors(authController.forgot));
router.get('/account/reset/:token', catchErrors(authController.reset));
router.post('/account/reset/:token',
  authController.confirmedPasswords,
  catchErrors(authController.update)
);
router.get('/map', imageController.mapPage);
router.get('/hearts', authController.isLoggedIn, catchErrors(imageController.getHearts));

// reviews
router.post('/reviews/:id',
  authController.isLoggedIn,
  catchErrors(reviewController.addReview)
);

// top
router.get('/top', catchErrors(imageController.getTopImages));


/*
  API
*/

router.get('/api/search', catchErrors(imageController.searchImages));
router.get('/api/images/near', catchErrors(imageController.mapImages));
router.post('/api/images/:id/heart', catchErrors(imageController.heartImage));

// remove image
router.post('/api/remove/:id', catchErrors(imageController.deleteImage));


module.exports = router;
